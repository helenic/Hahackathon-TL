import React from "react";
import {Switch, Route, Redirect} from "react-router"
import {HomePage} from "./components/HomePage/HomePage";

export const Routes = () => {

    return(
        <div>
            <Switch>
                <Route path={"/"} exact={true}>
                    <Redirect to={"home"}/>
                </Route>
                <Route path={"/home"} component={HomePage}/>
            </Switch>
        </div>
    );

}