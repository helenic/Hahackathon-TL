export interface CalculateResponse {
    text: string,
    humor: boolean,
    offenseRating: number | null,
    humorRating:number | null
}