import React from "react";
import "./JokeCalculation.css"

interface Props {
    text: string,
    humor: boolean,
    humorRating: number | null,
    offenseRating: number |null
}

export  const JokeCalculation = (props: Props) => {

    return (
        <div className="calculated-cnt">
            <div><label style={{width: "2.8rem", display:"block"}}>Text: </label><span style={{maxWidth: "500px"}}>{props.text}</span></div>
            <div style={{marginTop: "2rem"}}><label>Humoristic: </label><span>{props.humor ? "Yes" : "No"}</span></div>
            {props.humorRating !== null && <div><label>Humor rating: </label><span>{props.humorRating}<span className={"inner-label"}> / </span>5.0</span></div>}
            {props.offenseRating !== null && <div><label>Offense rating: </label><span>{props.offenseRating}<span className={"inner-label"}> / </span>5.0</span></div>}
        </div>
    );

}