import React, {useState} from "react";
import "./JokeInputField.css"
import {PulseLoader} from "react-spinners";
import {CalculateResponse} from "./CalculateResponse";
import {JokeCalculation} from "./JokeCalculation";

export const JokeInputField = () => {

    const [loading, setLoading] = useState(false);
    const [text, setText] = useState("");
    const [result, setResult]  =useState<CalculateResponse>({humor: false, humorRating: 0, offenseRating:0, text: ""})
    const [calculated, setCalculated] = useState<boolean>(false);

    const calculateHumor = async () => {
        setLoading(true);
        const response = await fetch("/hahackathon-api/calculate-humor?text=" + text);
        const json = await response.json();
        setLoading(false);
        setResult(json);
        console.log(json);
        if(response.status === 200)
        setCalculated(true);
        else {
            setCalculated(false);
        }
    }

    return (
        <div className="input_cnt">
            {!calculated ? <><PulseLoader loading={loading}/>
            <textarea name="joke-input-field"
                      value={text}
                      onChange={e => setText(e.target.value)}
                      className="joke-input-field"
                      placeholder="Insert a joke (5 words min)..."/>
            <button disabled={loading} className="calculate-humor-button"
                    onClick={calculateHumor}>Is it funny?
            </button></> : <div className="predicted-div">
                <JokeCalculation text={result.text} humor={result.humor} humorRating={result.humorRating} offenseRating={result.offenseRating}/>
                <button className="calculate-again-button" onClick={() => {
                    setCalculated(false);
                    setText("");
                }}>Try again</button>
            </div>}
        </div>
    );

}