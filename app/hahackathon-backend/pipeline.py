
import numpy as np
import spacy
from tensorflow.keras import models, optimizers, preprocessing as kprocessing
from tensorflow import keras
import pickle

nlp = spacy.load("en_core_web_sm")
tokenizer = pickle.load(open('tokenizer_glove300d_reduced.pickle', 'rb'))
ishumor_model = keras.models.load_model('ishumor_model_glove300d.h5')
humorrating_model = keras.models.load_model('humorrating_model_glove300d.h5')
offense_model = keras.models.load_model('offense_model_glove300d.h5')

def predict_for_text(text):
    text = text.lower()
    tokenized = nlp.make_doc(text)
    lemma = [token.lemma_ for token in tokenized]
    
    text_corpus = list(lemma)
    seq_text = np.hstack(tokenizer.texts_to_sequences(text_corpus)).astype(int)
    
    seq_text = np.reshape(seq_text, (1, -1))
    
    X_text = kprocessing.sequence.pad_sequences(seq_text
                                                 , maxlen=21
                                                 , padding="post"
                                                 , truncating="post")

    y_hat_ishumor = ishumor_model.predict(X_text)
    ishumor = np.argmax(y_hat_ishumor)
    
    humorrating = "[0]"
    if(ishumor == 1):
        humorrating = humorrating_model.predict(X_text)[0]
    
    offense=offense_model.predict(X_text)[0]
    
    print(f"{ishumor};{humorrating};{offense}")
	
text = input()
predict_for_text(text)