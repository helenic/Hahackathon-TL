package takelab.projekt.hahackathon.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HumorEvaluatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(HumorEvaluatorApplication.class, args);
    }

}
