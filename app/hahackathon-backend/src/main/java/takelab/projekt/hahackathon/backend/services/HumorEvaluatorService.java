package takelab.projekt.hahackathon.backend.services;

import org.springframework.lang.NonNull;
import takelab.projekt.hahackathon.backend.dtos.responses.HumorEvaluationResponse;

import java.io.IOException;

public interface HumorEvaluatorService {

    public HumorEvaluationResponse calculateHumor(@NonNull String text) throws IOException;

}
