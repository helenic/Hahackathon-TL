package takelab.projekt.hahackathon.backend.dtos.responses;

import org.springframework.lang.NonNull;

public class HumorEvaluationResponse {

    @NonNull
    private String text;

    private Boolean isHumor;

    private Double humorRating;

    private Double offenseRating;

    @NonNull
    public String getText() {
        return text;
    }

    public void setText(@NonNull String text) {
        this.text = text;
    }

    public Boolean getHumor() {
        return isHumor;
    }

    public void setHumor(Boolean humor) {
        isHumor = humor;
    }

    public Double getHumorRating() {
        return humorRating;
    }

    public void setHumorRating(Double humorRating) {
        this.humorRating = humorRating;
    }

    public Double getOffenseRating() {
        return offenseRating;
    }

    public void setOffenseRating(Double offenseRating) {
        this.offenseRating = offenseRating;
    }
}
